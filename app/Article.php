<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use \Carbon\Carbon;

class Article extends Model
{
    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);

        //$this->published_at = $this->published_at ?? Carbon::now();
    }
    
    public function scopeSelectPublished($query)
    {
        return $query->where('published_at', '<', Carbon::now());
    }

    public function getIsPublishedAtAttribute(): bool
    {
        return $this->published_at < Carbon::now();
    }

     public function is_published()
     {
        $res = $this->published_at;
        if ($this->published_at >= Carbon::now())  {
            $res = false;
        } else {
            $res = true;
        }
          
         return $res;
     }
    
    protected $fillable = ['title', 'description', 'test', 'published_at'];

    protected $attributes = [
       'test' => '33',
          
    ];

     protected $dates = [
        'published_at',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

// App\Article::create(['title'=>'tt', 'description' =>'tt2']);
//factory(App\Article::class, 2)->create();
}
