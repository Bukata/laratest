<?php
  use App\Article;
  use Illuminate\Http\Request;

  
  Route::get('/article', function () {
	 $article = Article::orderBy('created_at', 'asc')->get();
	  return view('article', [
		'article' => $article
	  ]);
  });
