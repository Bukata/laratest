<?php

use Faker\Generator as Faker;

$factory->define(App\Article::class, function (Faker $faker) {
    return [
        'title' => $faker->name,
        'description' => $faker->text,
        'published_at' => $faker->dateTimeInInterval( '-5 days', '+ 10 days'),
    ];
});
